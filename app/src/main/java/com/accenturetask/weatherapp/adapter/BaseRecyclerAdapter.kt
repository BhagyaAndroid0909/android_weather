package com.accenturetask.weatherapp.adapter

import androidx.recyclerview.widget.RecyclerView
import com.accenturetask.weatherapp.adapter.viewholder.BaseViewHolder


abstract class BaseRecyclerAdapter<T, V : BaseViewHolder<T, *>>(var data: MutableList<T>?) :
    RecyclerView.Adapter<V>() {

    protected var tag: String = javaClass.simpleName

    override fun onBindViewHolder(holder: V, position: Int) {
        holder.data = getItem(position)
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    @Throws(IndexOutOfBoundsException::class)
    fun getItem(position: Int): T {
        return data!![position]
    }


    @Throws(IndexOutOfBoundsException::class)
    fun removeItem(position: Int) {
        data!!.removeAt(position)
        notifyItemRemoved(position)
    }

}

