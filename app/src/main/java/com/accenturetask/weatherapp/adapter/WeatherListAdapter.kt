package com.accenturetask.weatherapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.accenturetask.weatherapp.R
import com.accenturetask.weatherapp.adapter.listener.BaseRecyclerListener
import com.accenturetask.weatherapp.adapter.viewholder.WeatherListViewHolder
import com.accenturetask.weatherapp.databinding.InflateWeathersListBinding
import com.accenturetask.weatherapp.repository.dto.response.WeatherData


class WeatherListAdapter(
    data: MutableList<WeatherData>?,
    private val listener: BaseRecyclerListener<WeatherData>
) : BaseRecyclerAdapter<WeatherData, WeatherListViewHolder>(data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherListViewHolder {
        return WeatherListViewHolder(
            DataBindingUtil.bind<ViewDataBinding>(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.inflate_weathers_list, parent, false)
            ) as InflateWeathersListBinding, listener
        )
    }
}
