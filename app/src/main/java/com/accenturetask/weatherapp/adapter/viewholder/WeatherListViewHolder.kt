package com.accenturetask.weatherapp.adapter.viewholder

import com.accenturetask.weatherapp.R
import com.accenturetask.weatherapp.adapter.listener.BaseRecyclerListener
import com.accenturetask.weatherapp.databinding.InflateWeathersListBinding
import com.accenturetask.weatherapp.repository.dto.response.WeatherData
import com.accenturetask.weatherapp.util.getDateTimeWeather
import com.accenturetask.weatherapp.util.getKelvinToCelsius

class WeatherListViewHolder(
    view: InflateWeathersListBinding,
    val listener: BaseRecyclerListener<WeatherData>
) : BaseViewHolder<WeatherData, InflateWeathersListBinding>(view) {
    override fun populateData(data: WeatherData) {
        viewDataBinding.data = data
        viewDataBinding.city = data.name
        viewDataBinding.temp =
            getKelvinToCelsius(data.main.temp).toString() + itemView.context.getString(R.string.celcious_symbol)
        viewDataBinding.position = adapterPosition
        viewDataBinding.listener = listener
        viewDataBinding.time = getDateTimeWeather(data.dt)

    }

}