package com.accenturetask.weatherapp.adapter.listener

interface BaseRecyclerListener<T> {
    fun onClickItem(data: T, position: Int)
}