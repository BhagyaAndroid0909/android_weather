package com.accenturetask.weatherapp.adapter.viewholder

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView


abstract class BaseViewHolder<T, VB : ViewDataBinding> internal constructor(protected var viewDataBinding: VB) :
    RecyclerView.ViewHolder(viewDataBinding.root) {

    var data: T? = null
        set(value) {
            field = value
            data?.let { populateData(it) }
        }

    internal var tag = javaClass.simpleName


    internal abstract fun populateData(data: T)

}
