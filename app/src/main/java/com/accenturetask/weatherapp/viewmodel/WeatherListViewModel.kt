package com.accenturetask.weatherapp.viewmodel

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.accenturetask.library.CustomException
import com.accenturetask.weatherapp.common.Constants
import com.accenturetask.weatherapp.common.Constants.Common.Companion.IS_CITY
import com.accenturetask.weatherapp.common.Constants.Common.Companion.WEATHER_DATA
import com.accenturetask.weatherapp.repository.OtherRepository
import com.accenturetask.weatherapp.repository.dto.response.WeatherCityResponse
import com.accenturetask.weatherapp.repository.dto.response.WeatherData
import com.accenturetask.weatherapp.repository.dto.response.WeatherListResponse
import io.reactivex.android.schedulers.AndroidSchedulers


class WeatherListViewModel : BaseViewModel() {

    val weatherResponse = MutableLiveData<WeatherListResponse>()
    val weatherCityResponse = MutableLiveData<WeatherCityResponse>()
    val weatherData = MutableLiveData<WeatherData>()
    var isCity: Boolean = false

    override fun onViewCreated(extras: Bundle?) {

    }

    override fun onCreate(bundle: Bundle?) {
        weatherData.value = bundle?.getParcelable(WEATHER_DATA)
        isCity = bundle?.getBoolean(IS_CITY) == true
    }

    @SuppressLint("CheckResult")
    fun getWeatherReport(lati: String?, longi: String?, appId: String?) {
        OtherRepository().getWeatherDetailsList(lati, longi, 50, appId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                it?.apply {
                    weatherResponse.value = this
                }
            }, {
                if (it is CustomException) {
                    dialogObservable.onNext(it)
                } else
                    messageObserver.value = it.message
            })
    }

    @SuppressLint("CheckResult")
    fun getCityWeather(city: String?, units: String?, appId: String?) {
        OtherRepository().getCityWeather(city, units, appId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                it?.apply {
                    weatherCityResponse.value = this
                }
            }, {
                if (it is CustomException) {
                    dialogObservable.onNext(it)
                } else
                    messageObserver.value = it.message
            })
    }

    fun insertIntoDatabase(data: List<WeatherData>?) {
        OtherRepository().insertWeatherListIntoDatabase(data)
    }

    fun databaseAccess(): List<WeatherData>? {
        return OtherRepository().getLocalDbWeatherData()
    }


}