package com.accenturetask.weatherapp.viewmodel


import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.accenturetask.library.CustomException
import com.accenturetask.weatherapp.common.WeatherApplication
import io.reactivex.subjects.PublishSubject

abstract class BaseViewModel : ViewModel() {

    open fun onActivityForResult(requestCode: Int, resultCode: Int, data: Bundle?) {

    }

    val dialogObservable: PublishSubject<CustomException> = PublishSubject.create()
    val messageObserver = MutableLiveData<String>()

    init {
        WeatherApplication.appComponent.inject(this)
    }

    abstract fun onViewCreated(extras: Bundle?)

    abstract fun onCreate(bundle: Bundle?)
}