package com.accenturetask.weatherapp.repository.dto.response


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "wind")
data class Wind(
    @PrimaryKey
    var id: Int = 0,
    var deg: Int = 0,
    var speed: Double = 0.0
): Parcelable