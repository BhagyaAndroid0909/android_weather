package com.accenturetask.weatherapp.repository.dto.response


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "coord")
data class Coord(
    @PrimaryKey
    var id: Int = 0,
    var lat: Double = 0.0,
    var lon: Double = 0.0
) : Parcelable