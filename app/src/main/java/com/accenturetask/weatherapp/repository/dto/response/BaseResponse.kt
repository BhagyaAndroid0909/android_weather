package com.accenturetask.weatherapp.repository.dto.response


open class BaseResponse(
    open var message: String = "Success",
)