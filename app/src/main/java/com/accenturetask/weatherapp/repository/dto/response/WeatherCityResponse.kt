package com.accenturetask.weatherapp.repository.dto.response



data class WeatherCityResponse(
    var clouds: Clouds = Clouds(),
    var coord: Coord = Coord(),
    var dt: Long = 0,
    var id: Int = 0,
    var main: Main = Main(),
    var name: String = "",
    var sys: Sys = Sys(),
    var weather: MutableList<Weather> = ArrayList(),
    var wind: Wind = Wind()
) : BaseResponse()