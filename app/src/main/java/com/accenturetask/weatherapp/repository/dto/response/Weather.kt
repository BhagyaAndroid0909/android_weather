package com.accenturetask.weatherapp.repository.dto.response


import android.os.Parcelable
import androidx.room.Entity
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "weather")

@Parcelize
data class Weather(
    var description: String = "",
    var icon: String = "",
    var id: Int = 0,
    var main: String = ""
): Parcelable