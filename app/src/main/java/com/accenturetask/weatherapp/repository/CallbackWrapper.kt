package com.neibors.repository


import com.accenturetask.library.CustomException
import com.accenturetask.library.Log
import com.accenturetask.weatherapp.common.Constants.HttpErrorMessage.Companion.FORBIDDEN
import com.accenturetask.weatherapp.common.Constants.HttpErrorMessage.Companion.INTERNAL_SERVER_ERROR
import com.accenturetask.weatherapp.common.Constants.HttpErrorMessage.Companion.TIMEOUT
import com.accenturetask.weatherapp.common.Constants.HttpErrorMessage.Companion.UNAUTHORIZED
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.BAD_REQUEST_CODE
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.FAILURE_CODE
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.INTERNAL_SERVER_ERROR_CODE
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.TIMEOUT_CODE
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.UNAUTHORIZED_CODE
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.VERIFY_FAILURE_CODE
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import com.accenturetask.weatherapp.di.InjectorHelper
import com.accenturetask.weatherapp.repository.dto.response.BaseResponse
import com.accenturetask.weatherapp.repository.dto.response.ErrorResponse
import io.reactivex.observers.DisposableObserver
import okhttp3.ResponseBody
import retrofit2.Converter
import java.io.IOException
import java.net.SocketTimeoutException

abstract class CallbackWrapper<T : BaseResponse> : DisposableObserver<T>() {

    private val injectorHelper: InjectorHelper = InjectorHelper()
    private val tag = javaClass.simpleName

    override fun onComplete() {

    }

    override fun onNext(t: T) {
        success(t)
    }


    override fun onError(e: Throwable) {
        Log.d("onError: $e")

        if (e is HttpException) {
            when {
                e.code() == UNAUTHORIZED_CODE -> failure(
                    CustomException(
                        UNAUTHORIZED_CODE,
                        UNAUTHORIZED
                    )
                )
                e.code() == INTERNAL_SERVER_ERROR_CODE -> failure(
                    CustomException(
                        INTERNAL_SERVER_ERROR_CODE,
                        INTERNAL_SERVER_ERROR
                    )
                )
                e.code() == FAILURE_CODE -> failure(parseError(e))
                e.code() == VERIFY_FAILURE_CODE -> failure(parseError(e))
            }
        } else if (e is SocketTimeoutException) {
            failure(CustomException(TIMEOUT_CODE, TIMEOUT))
        } else if (e is IOException) {
            failure(CustomException(BAD_REQUEST_CODE, FORBIDDEN))
        } else {
            failure(CustomException(INTERNAL_SERVER_ERROR_CODE, INTERNAL_SERVER_ERROR))
        }
    }

    private fun parseError(throwable: HttpException?): CustomException {
        val converter: Converter<ResponseBody, ErrorResponse> =
            injectorHelper.apiClient.retrofit.responseBodyConverter(
                ErrorResponse::class.java,
                emptyArray()
            )
        return CustomException(
            throwable?.response()!!.code(),
            converter.convert(throwable.response().errorBody()!!)!!.message
        )
    }


    abstract fun success(response: T)

    abstract fun failure(e: Throwable)
}