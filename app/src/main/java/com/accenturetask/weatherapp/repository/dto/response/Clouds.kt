package com.accenturetask.weatherapp.repository.dto.response

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "clouds")
data class Clouds(
    @PrimaryKey
    var all: Int = 0
): Parcelable