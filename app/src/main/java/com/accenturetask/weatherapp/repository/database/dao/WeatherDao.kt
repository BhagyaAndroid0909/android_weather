package com.accenturetask.weatherapp.repository.database.dao


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.accenturetask.weatherapp.repository.dto.response.WeatherData

@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(word: List<WeatherData>?)

    @Query("SELECT * from weatherdata")
    fun getWeatherDataList(): List<WeatherData>

}