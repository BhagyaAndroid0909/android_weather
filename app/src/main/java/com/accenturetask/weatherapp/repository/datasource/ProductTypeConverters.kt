package com.accenturetask.weatherapp.repository.datasource

import androidx.room.TypeConverter
import com.accenturetask.weatherapp.repository.dto.response.*
import com.accenturetask.weatherapp.util.CodeSnippet


class ProductTypeConverters {

    //Weather List converter
    @TypeConverter
    fun stringToWeathers(json: String): List<Weather>? {
        return CodeSnippet.getListFromJsonString(json, Weather::class.java)
    }

    @TypeConverter
    fun weathersToString(list: List<Weather>): String? {

        return CodeSnippet.getJsonStringFromListObject(list, Weather::class.java)
    }

    //Clouds converter
    @TypeConverter
    fun stringToClouds(json: String): Clouds? {
        return CodeSnippet.getObjectFromJsonString(json, Clouds::class.java)
    }

    @TypeConverter
    fun cloudsToString(clouds: Clouds): String? {

        return CodeSnippet.getJsonStringFromObject(clouds, Clouds::class.java)
    }

    //Coord converter
    @TypeConverter
    fun stringToCoord(json: String): Coord? {
        return CodeSnippet.getObjectFromJsonString(json, Coord::class.java)
    }

    @TypeConverter
    fun coordToString(coord: Coord): String? {
        return CodeSnippet.getJsonStringFromObject(coord, Coord::class.java)
    }

    //Coord converter
    @TypeConverter
    fun stringToMain(json: String): Main? {
        return CodeSnippet.getObjectFromJsonString(json, Main::class.java)
    }

    @TypeConverter
    fun mainToString(main: Main): String? {
        return CodeSnippet.getJsonStringFromObject(main, Main::class.java)
    }

    //Sys converter
    @TypeConverter
    fun stringToSys(json: String): Sys? {
        return CodeSnippet.getObjectFromJsonString(json, Sys::class.java)
    }

    @TypeConverter
    fun sysToString(sys: Sys): String? {
        return CodeSnippet.getJsonStringFromObject(sys, Sys::class.java)
    }

    //Wind converter
    @TypeConverter
    fun stringToWind(json: String): Wind? {
        return CodeSnippet.getObjectFromJsonString(json, Wind::class.java)
    }

    @TypeConverter
    fun windToString(main: Wind): String? {
        return CodeSnippet.getJsonStringFromObject(main, Wind::class.java)
    }


}