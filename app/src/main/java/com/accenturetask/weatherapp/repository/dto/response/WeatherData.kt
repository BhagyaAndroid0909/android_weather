package com.accenturetask.weatherapp.repository.dto.response


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "weatherdata")
data class WeatherData(
    var clouds: Clouds = Clouds(),
    var coord: Coord = Coord(),
    var dt: Long = 0,
    @PrimaryKey
    var id: Int = 0,
    var main: Main = Main(),
    var name: String = "",
    var sys: Sys = Sys(),
    var weather: MutableList<Weather> =ArrayList(),
    var wind: Wind = Wind()
): Parcelable