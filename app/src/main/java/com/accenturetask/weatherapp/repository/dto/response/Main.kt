package com.accenturetask.weatherapp.repository.dto.response


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "main")
data class Main(
    @PrimaryKey
    var id: Int = 0,
    @field:Json(name = "feels_like")
    var feelsLike: Float = 0F,
    @Json(name = "grnd_level")
    var grndLevel: Int = 0,
    var humidity: Int = 0,
    var pressure: Int = 0,
    @Json(name = "sea_level")
    var seaLevel: Int = 0,
    var temp: Float = 0F,
    @field:Json(name = "temp_max")
    var tempMax: Float = 0F,
    @field:Json(name = "temp_min")
    var tempMin: Float = 0F
): Parcelable