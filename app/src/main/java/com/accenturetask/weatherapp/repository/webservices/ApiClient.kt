package com.accenturetask.weatherapp.repository.webservices

import com.accenturetask.weatherapp.BuildConfig
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.accenturetask.weatherapp.common.WeatherApplication
import com.accenturetask.weatherapp.util.CodeSnippet
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ApiClient {

    @Inject
    lateinit var codeSnippet: CodeSnippet
    val retrofit: Retrofit

    init {
        WeatherApplication.weatherApplication!!.getUserAppComponent().inject(this)
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method(), original.body())
                .build()
            chain.proceed(request)
        }

        retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(httpClient.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }

    fun apiInterface(): ApiInterface = retrofit.create(ApiInterface::class.java)

}