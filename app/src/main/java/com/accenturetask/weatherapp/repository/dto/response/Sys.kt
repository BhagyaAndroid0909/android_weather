package com.accenturetask.weatherapp.repository.dto.response


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "sys")
data class Sys(
    @PrimaryKey
    var id: Int = 0,
    var country: String = ""
):Parcelable