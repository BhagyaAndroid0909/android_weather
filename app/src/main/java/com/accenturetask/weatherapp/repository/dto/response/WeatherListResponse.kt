package com.accenturetask.weatherapp.repository.dto.response



data class WeatherListResponse(
    var count: Int = 0,
    var list: List<WeatherData> = listOf(),
) : BaseResponse()