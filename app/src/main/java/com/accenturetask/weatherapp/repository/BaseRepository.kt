package com.accenturetask.weatherapp.repository

import android.content.Context
import com.accenturetask.library.CustomException
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.BAD_REQUEST_CODE
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.FAILURE_CODE
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.INTERNAL_SERVER_ERROR_CODE
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.TIMEOUT_CODE
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.UNAUTHORIZED_CODE
import com.accenturetask.weatherapp.common.Constants.InternalHttpCode.Companion.VERIFY_FAILURE_CODE
import com.accenturetask.weatherapp.common.WeatherApplication
import com.accenturetask.weatherapp.repository.dto.response.BaseResponse
import com.accenturetask.weatherapp.repository.webservices.ApiClient
import com.accenturetask.weatherapp.util.logd
import com.neibors.repository.CallbackWrapper
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


open class BaseRepository {

    @Inject
    protected
    lateinit var apiClient: ApiClient

    @Inject
    lateinit var context: Context

    init {
        WeatherApplication.weatherApplication!!.getUserAppComponent().inject(this)
    }


    protected fun <T : BaseResponse> enqueue(observable: Observable<T>): Observable<T> {
        return Observable.create { emitter: ObservableEmitter<T> ->
            run {
                observable.subscribeOn(Schedulers.newThread())
                    .subscribeWith(object : CallbackWrapper<T>() {
                        override fun success(response: T) {
                            logd("Success: " + response.message)
                            emitter.onNext(response)
                        }


                        override fun failure(e: Throwable) {
                            logd("failure: $e")
                            when ((e as CustomException).code) {
                                TIMEOUT_CODE -> {
                                    emitter.onError(Throwable(e.exception))
                                }
                                BAD_REQUEST_CODE -> {
                                    emitter.onError(Throwable(e.exception))
                                }
                                INTERNAL_SERVER_ERROR_CODE -> {
                                    emitter.onError(Throwable(e.exception))
                                }
                                UNAUTHORIZED_CODE -> {
                                    emitter.onError(e)
                                }
                                FAILURE_CODE -> {
                                    emitter.onError(Throwable(e.exception))
                                }
                                VERIFY_FAILURE_CODE -> {
                                    emitter.onError(Throwable(e.exception))
                                }
                                else -> emitter.onError(Throwable(e.exception))
                            }


                        }
                    })
            }
        }

    }


}