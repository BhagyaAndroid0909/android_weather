package com.accenturetask.weatherapp.repository.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.accenturetask.weatherapp.repository.database.dao.WeatherDao
import com.accenturetask.weatherapp.repository.datasource.ProductTypeConverters
import com.accenturetask.weatherapp.repository.dto.response.*


@Database(
    entities = [WeatherData::class, Clouds::class, Coord::class, Main::class, Sys::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(ProductTypeConverters::class)
abstract class WeatherDatabase : RoomDatabase() {

    abstract fun getWeatherDao(): WeatherDao

    companion object {
        private var INSTANCE: WeatherDatabase? = null

        /* migrate the database*/
//        private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
//            override fun migrate(database: SupportSQLiteDatabase) {
//                // Since we didn't alter the table, there's nothing else to do here.
//            }
//        }

        fun getInstance(context: Context): WeatherDatabase? {
            if (INSTANCE == null) {
                synchronized(WeatherDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        WeatherDatabase::class.java, "weather.db"
                    )
                        .allowMainThreadQueries()
//                        .addMigrations(MIGRATION_1_2)
                        .build()
                }
            }
            return INSTANCE
        }

    }


}