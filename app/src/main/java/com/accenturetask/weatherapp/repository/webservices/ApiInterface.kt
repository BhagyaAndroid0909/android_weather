package com.accenturetask.weatherapp.repository.webservices

import com.accenturetask.weatherapp.repository.dto.response.WeatherCityResponse
import com.accenturetask.weatherapp.repository.dto.response.WeatherListResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiInterface {

    /* Current location Weather List */
    @GET("data/2.5/find")
    fun getWeatherDetailsList(
        @Query("lat") latitude: String?,
        @Query("lon") longitude: String?,
        @Query("cnt") count: Int,
        @Query("appid") appId: String?
    ): Observable<WeatherListResponse>

    /* searched City Weather */
    @GET("data/2.5/weather")
    fun getCityWeather(
        @Query("q") city: String?,
        @Query("units") unit: String?,
        @Query("appid") appId: String?
    ): Observable<WeatherCityResponse>
}