package com.accenturetask.weatherapp.repository

import com.accenturetask.weatherapp.repository.database.WeatherDatabase
import com.accenturetask.weatherapp.repository.dto.response.WeatherData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class OtherRepository : BaseRepository() {

    /*SERVER METHOD*/
    var database = WeatherDatabase.getInstance(context)

    fun getWeatherDetailsList(lat: String?, log: String?, count: Int, appId: String?) =
        enqueue(apiClient.apiInterface().getWeatherDetailsList(lat, log, count, appId))

    fun getCityWeather(cityName: String?, units: String?, appId: String?) =
        enqueue(apiClient.apiInterface().getCityWeather(cityName, units, appId))

    /*LOCAL DB METHOD*/
    fun getLocalDbWeatherData(): List<WeatherData>? {
        return database?.getWeatherDao()?.getWeatherDataList()
    }

    fun insertWeatherListIntoDatabase(response: List<WeatherData>?) {
        Single.fromCallable {
            database?.getWeatherDao()?.insert(response)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe()
    }

}