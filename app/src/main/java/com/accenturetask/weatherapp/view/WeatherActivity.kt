package com.accenturetask.weatherapp.view.activity

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.accenturetask.weatherapp.R
import com.accenturetask.weatherapp.adapter.WeatherListAdapter
import com.accenturetask.weatherapp.adapter.listener.BaseRecyclerListener
import com.accenturetask.weatherapp.common.Constants.Common.Companion.IS_CITY
import com.accenturetask.weatherapp.common.Constants.Common.Companion.PERIODIC_DELAY_MILLIS
import com.accenturetask.weatherapp.common.Constants.Common.Companion.WEATHER_DATA
import com.accenturetask.weatherapp.databinding.ActivityWeatherBinding
import com.accenturetask.weatherapp.repository.dto.response.WeatherCityResponse
import com.accenturetask.weatherapp.repository.dto.response.WeatherData
import com.accenturetask.weatherapp.util.CodeSnippet
import com.accenturetask.weatherapp.util.getDateTimeWeather
import com.accenturetask.weatherapp.util.logd
import com.accenturetask.weatherapp.view.WeatherDetailsActivity
import com.accenturetask.weatherapp.viewmodel.WeatherListViewModel
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import kotlin.math.roundToInt

class WeatherActivity :
    BaseActivity<WeatherListViewModel, ActivityWeatherBinding>(),
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    LocationListener, View.OnClickListener {

    private var weatherListAdapter: WeatherListAdapter? = null
    private var weatherList: ArrayList<WeatherData>? = ArrayList()
    private var cityWeather: WeatherCityResponse = WeatherCityResponse()
    private var service: LocationManager? = null
    private var enabled: Boolean? = null
    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLastLocation: Location? = null
    private var REQUEST_LOCATION_CODE = 101

    override fun initializeView(): WeatherListViewModel {
        service = this.getSystemService(LOCATION_SERVICE) as LocationManager
        enabled = service!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        return ViewModelProviders.of(this).get(WeatherListViewModel::class.java)
    }

    override fun layoutRes(): Int {
        return R.layout.activity_weather
    }

    override fun initializeListener() {
        viewBinding.ivCurrentLoc.setOnClickListener(this)
        viewBinding.cardCityData.setOnClickListener(this)
        onClickCurrentLoc()
        viewBinding.searchView.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.isEmpty()) {
                    viewBinding.isSearch = false
                } else {
                    viewBinding.isSearch = true
                    getCityWeather(s.toString().trim())
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                viewBinding.isSearch = s.isNotEmpty()
            }
        })
    }

    private fun setListAdapter(weatherList: MutableList<WeatherData>?) {
        weatherListAdapter =
            WeatherListAdapter(
                weatherList,
                clickListener
            )
        val linearLayoutManager = LinearLayoutManager(this)
        viewBinding.rvWeatherList.layoutManager = linearLayoutManager
        viewBinding.rvWeatherList.adapter = weatherListAdapter
    }


    override fun onViewModelCreated() {

        viewModel.weatherResponse.observe(this, Observer { data ->
            data.apply {
                weatherList?.addAll(this.list)
                setListAdapter(weatherList)
                viewModel.insertIntoDatabase(this.list)
            }
        })

        viewModel.weatherCityResponse.observe(this, Observer { data ->
            data.apply {
                viewBinding.cityData = this
                cityWeather = this
                viewBinding.cardCityData.isClickable = true
                viewBinding.time = getDateTimeWeather(this.dt)
                viewBinding.temp =
                    data.main.temp.roundToInt().toString() + getString(R.string.celcious_symbol)
            }
        })

        viewModel.messageObserver.observe(this, Observer {
            if (it != null) {
                viewBinding.cardCityData.isClickable = false
                logd(it)
                //showMessage(it)
            }
        })
    }

    private fun getWeatherList(lati: String?, longi: String?) {
        if (CodeSnippet(applicationContext).isNetworkConnectionAvailable()) {
            viewModel.getWeatherReport(lati, longi, getString(R.string.app_id))
        } else if (!CodeSnippet(this).isNetworkConnectionAvailable()) {
            //get from database
            databaseAccess()
        }
    }

    private fun getCityWeather(city: String?) {
        if (CodeSnippet(applicationContext).isNetworkConnectionAvailable()) {
            viewModel.getCityWeather(city, getString(R.string.units), getString(R.string.app_id))
        } else {
            showMessage(resources.getString(R.string.alert_no_internet))
        }
    }

    private fun databaseAccess() {
        val groups = viewModel.databaseAccess()
        if (groups != null) {
            weatherList?.clear()
            weatherList?.addAll(groups.toMutableList())
            setListAdapter(weatherList)
        }
    }

    private val clickListener = object : BaseRecyclerListener<WeatherData> {

        override fun onClickItem(data: WeatherData, position: Int) {
            navigateToWeatherDetails(data)
        }

    }

    private fun navigateToWeatherDetails(data: WeatherData) {
        val intent = Intent(this@WeatherActivity, WeatherDetailsActivity::class.java)
        intent.putExtra(WEATHER_DATA, data)
        intent.putExtra(IS_CITY, false)
        startActivity(intent)
    }

    private fun navigateToCityWeatherDetails(data: WeatherCityResponse) {
        val intent = Intent(this@WeatherActivity, WeatherDetailsActivity::class.java)
        val weatherData = WeatherData()
        weatherData.name = data.name
        weatherData.main.temp = data.main.temp
        weatherData.main.tempMin = data.main.tempMin
        weatherData.main.tempMax = data.main.tempMax
        weatherData.sys.country = data.sys.country
        weatherData.main.humidity = data.main.humidity
        weatherData.main.feelsLike = data.main.feelsLike
        weatherData.main.pressure = data.main.pressure
        intent.putExtra(WEATHER_DATA, weatherData)
        intent.putExtra(IS_CITY, true)
        startActivity(intent)
    }

    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = PERIODIC_DELAY_MILLIS
        mLocationRequest!!.fastestInterval = PERIODIC_DELAY_MILLIS
        mLocationRequest!!.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY

        // Check if enabled and if not send user to the GPS settings
        if (!enabled!!) {
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent)
        }
        // Check if permission is granted or not
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            )
        }
    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onLocationChanged(location: Location) {
        mLastLocation = location
        getWeatherList(location.latitude.toString(), location.longitude.toString())
    }

    @Synchronized
    fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        mGoogleApiClient!!.connect()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_LOCATION_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient()
                        }
                    }
                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show()
                }
                return
            }
        }
    }

    private fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                AlertDialog.Builder(this)
                    .setTitle("Location Permission Needed")
                    .setMessage("This app needs the Location permission, please accept to use location functionality")
                    .setPositiveButton("OK") { _, _ ->
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            REQUEST_LOCATION_CODE
                        )
                    }
                    .create()
                    .show()

            } else ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_LOCATION_CODE
            )
        }
    }

    private fun onClickCurrentLoc() {
        viewBinding.searchView.text?.clear()
        viewBinding.isSearch = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                //Location Permission already granted
                buildGoogleApiClient()
            } else {
                //Request Location Permission
                checkLocationPermission()
            }
        } else {
            buildGoogleApiClient()
        }
    }

    override fun onClick(p0: View?) {
        when (p0) {
            viewBinding.ivCurrentLoc -> {
                onClickCurrentLoc()
            }
            viewBinding.cardCityData -> {
                navigateToCityWeatherDetails(cityWeather)
            }
        }
    }

}
