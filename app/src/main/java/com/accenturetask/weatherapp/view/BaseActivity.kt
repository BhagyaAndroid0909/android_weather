package com.accenturetask.weatherapp.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.accenturetask.weatherapp.viewmodel.BaseViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


abstract class BaseActivity<ViewModel : BaseViewModel, ViewBinding : ViewDataBinding> :
    AppCompatActivity() {

    protected lateinit var viewBinding: ViewBinding
    protected var tag: String = javaClass.simpleName
    lateinit var dialog: AlertDialog
    private var compositeDisposable = CompositeDisposable()

    protected val viewModel: ViewModel by lazy {
        initializeView()
    }

    private fun addToDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    fun addToDisposables(disposable: Disposable) {
        compositeDisposable.addAll(disposable)
    }


    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = DataBindingUtil.setContentView(this, layoutRes())
        viewModel.onCreate(intent.extras).apply {
            onViewModelCreated()
        }
        viewModel.messageObserver.observe(this, {
        })
        viewModel.dialogObservable.subscribe {
        }
        initializeListener()
    }

    fun setToolbar(toolbar: Toolbar) {
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = ""
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        viewModel.onActivityForResult(requestCode, resultCode, data?.extras)
    }

    abstract fun initializeView(): ViewModel

    abstract fun layoutRes(): Int

    abstract fun initializeListener()

    open fun onViewModelCreated() {

    }

    override fun onNavigateUp(): Boolean {
        onBackPressed()
        return super.onNavigateUp()
    }

    fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }


    fun dialogDismiss() {
        dialog.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}