package com.accenturetask.weatherapp.view

import androidx.lifecycle.ViewModelProviders
import com.accenturetask.weatherapp.R
import com.accenturetask.weatherapp.databinding.ActivityWeatherDetailBinding
import com.accenturetask.weatherapp.util.getKelvinToCelsius
import com.accenturetask.weatherapp.view.activity.BaseActivity
import com.accenturetask.weatherapp.viewmodel.WeatherListViewModel
import kotlin.math.roundToInt

class WeatherDetailsActivity :
    BaseActivity<WeatherListViewModel, ActivityWeatherDetailBinding>() {

    override fun initializeView(): WeatherListViewModel {

        return ViewModelProviders.of(this).get(WeatherListViewModel::class.java)
    }

    override fun layoutRes(): Int {
        return R.layout.activity_weather_detail
    }

    override fun initializeListener() {

    }

    override fun onViewModelCreated() {
        viewModel.weatherData.observe(this, { data ->
            data.apply {
                viewBinding.data = this
                if (viewModel.isCity) {
                    viewBinding.temp =
                        data.main.temp.roundToInt().toString() + getString(R.string.celcious_symbol)
                    viewBinding.feelTemp = data.main.feelsLike.roundToInt()
                        .toString() + getString(R.string.celcious_symbol)
                    viewBinding.tempMin = data.main.tempMin.roundToInt()
                        .toString() + getString(R.string.celcious_symbol)
                    viewBinding.tempMax = data.main.tempMin.roundToInt()
                        .toString() + getString(R.string.celcious_symbol)
                } else if (!viewModel.isCity) {
                    viewBinding.temp =
                        getKelvinToCelsius(data.main.temp).toString() + getString(R.string.celcious_symbol)
                    viewBinding.feelTemp =
                        getKelvinToCelsius(data.main.feelsLike).toString() + getString(R.string.celcious_symbol)
                    viewBinding.tempMin =
                        getKelvinToCelsius(data.main.tempMin).toString() + getString(R.string.celcious_symbol)
                    viewBinding.tempMax =
                        getKelvinToCelsius(data.main.tempMin).toString() + getString(R.string.celcious_symbol)
                }
                viewBinding.pressure =
                    data.main.pressure.toString() + getString(R.string.pressure_unit)
                viewBinding.hum = data.main.humidity.toString()
            }
        })
    }

}
