package com.accenturetask.weatherapp.util

import com.accenturetask.library.ExceptionTracker
import com.accenturetask.library.Log
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt


fun logd(message: String) {
    Log.d(message)
}


fun logd(e: Throwable) {
    Log.d(e.message!!)
}


fun getDateTimeWeather(time: Long): String? {
    val timeLong = time * 1000
    try {
        val updatedTime: String =
            SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH).format(Date(timeLong))
        return updatedTime
    } catch (e: Exception) {
        ExceptionTracker.track(e)
    }
    return null
}

fun getKelvinToCelsius(time: Float): Int {
    return (time - 273.15).roundToInt()
}


