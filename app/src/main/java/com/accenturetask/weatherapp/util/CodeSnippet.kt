package com.accenturetask.weatherapp.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.io.IOException
import javax.inject.Singleton


@Singleton
open class CodeSnippet(val context: Context) {

    fun isNetworkConnectionAvailable(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = cm.activeNetworkInfo ?: return false
        val network = info.state
        return network === NetworkInfo.State.CONNECTED || network === NetworkInfo.State.CONNECTING
    }


    companion object {
        fun <T> getObjectFromJsonString(jsonString: String?, classType: Class<T>): T? {

            if (jsonString == null)
                return null

            try {
                val moshi: Moshi = Moshi.Builder().build()
                return moshi.adapter(classType).fromJson(jsonString)
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

            return null
        }

        fun <T> getJsonStringFromObject(`object`: T?, classType: Class<T>): String? {

            if (`object` == null)
                return null

            try {
                val moshi: Moshi = Moshi.Builder().build()
                return moshi.adapter(classType).toJson(`object`)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }


        fun <T> getListFromJsonString(jsonString: String, classType: Class<T>): List<T>? {
            try {
                val moshi: Moshi = Moshi.Builder().build()
                return moshi.adapter<List<T>>(
                    Types.newParameterizedType(
                        List::class.java,
                        classType
                    )
                )
                    .fromJson(jsonString)
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

            return null
        }

        fun <T> getJsonStringFromListObject(data: List<T>?, classType: Class<T>): String? {

            if (data == null)
                return null

            try {
                val moshi: Moshi = Moshi.Builder().build()
                return moshi.adapter<List<T>>(
                    Types.newParameterizedType(
                        List::class.java,
                        classType
                    )
                ).toJson(data)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }


    }
}

