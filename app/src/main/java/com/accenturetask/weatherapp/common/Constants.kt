package com.accenturetask.weatherapp.common


interface Constants {


    interface InternalHttpCode {
        companion object {
            const val VERIFY_FAILURE_CODE = 202
            const val FAILURE_CODE = 404
            const val BAD_REQUEST_CODE = 400
            const val UNAUTHORIZED_CODE = 401
            const val INTERNAL_SERVER_ERROR_CODE = 500
            const val TIMEOUT_CODE = 504
        }
    }

    interface HttpErrorMessage {
        companion object {
            const val INTERNAL_SERVER_ERROR =
                "Our server is under maintenance. We will resolve shortly!"
            const val FORBIDDEN = "Seems like you haven't permitted to do this operation!"
            const val TIMEOUT = "Unable to connect to server. Please try after sometime"
            const val UNAUTHORIZED = "UnAuthorized"
        }
    }

    interface Common {
        companion object {
            const val PERIODIC_DELAY_MILLIS = 600000L //// 10 minutes interval
            const val WEATHER_DATA = "weather_data"
            const val IS_CITY = "is_city"
        }
    }

}
