package com.accenturetask.weatherapp.common

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.accenturetask.library.CrashReportingTree
import com.accenturetask.weatherapp.BuildConfig
import com.accenturetask.weatherapp.di.AppComponent
import com.accenturetask.weatherapp.di.AppModule
import com.accenturetask.weatherapp.di.DaggerAppComponent
import com.accenturetask.weatherapp.util.deleteCache
import timber.log.Timber
import timber.log.Timber.DebugTree


class WeatherApplication : MultiDexApplication() {
    private lateinit var mContext: Context


    override fun onCreate() {
        super.onCreate()
        weatherApplication = this@WeatherApplication
        mContext = applicationContext
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
        appComponent.inject(this)
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
        deleteCache(mContext)
    }


    fun getUserAppComponent(): AppComponent {
        return appComponent
    }

    companion object {
        lateinit var appComponent: AppComponent

        var weatherApplication: WeatherApplication? = null
            private set
    }


}
