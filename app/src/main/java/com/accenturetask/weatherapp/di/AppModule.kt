package com.accenturetask.weatherapp.di

import android.content.Context
import com.accenturetask.weatherapp.repository.webservices.ApiClient
import com.accenturetask.weatherapp.util.CodeSnippet
import com.accenturetask.weatherapp.di.annotation.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class AppModule(@ApplicationContext @Named("applicationContext") private val context: Context) {

    @Provides
    fun provideApplicationContext() = context

    @Provides
    fun provideCodeSnippet() = CodeSnippet(context)


    @Provides
    fun provideApiClient(): ApiClient {
        return ApiClient()
    }
}