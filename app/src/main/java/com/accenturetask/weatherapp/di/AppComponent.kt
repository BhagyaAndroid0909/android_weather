package com.accenturetask.weatherapp.di

import com.accenturetask.weatherapp.common.WeatherApplication
import com.accenturetask.weatherapp.repository.webservices.ApiClient
import com.accenturetask.weatherapp.viewmodel.BaseViewModel
import com.accenturetask.weatherapp.repository.BaseRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(appController: WeatherApplication)
    fun inject(baseViewModel: BaseViewModel)
    fun inject(injectorHelper: InjectorHelper)
    fun inject(apiClient: ApiClient)
    fun inject(baseRepository: BaseRepository)
}