package com.accenturetask.weatherapp.di


import com.accenturetask.weatherapp.common.WeatherApplication
import com.accenturetask.weatherapp.repository.webservices.ApiClient
import com.accenturetask.weatherapp.util.CodeSnippet
import javax.inject.Inject

class InjectorHelper {

    @Inject
    lateinit var codeSnippet: CodeSnippet

    @Inject
    lateinit var apiClient: ApiClient

    init {
        WeatherApplication.weatherApplication!!.getUserAppComponent().inject(this)
    }

}