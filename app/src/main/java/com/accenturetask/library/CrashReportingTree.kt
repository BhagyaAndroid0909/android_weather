package com.accenturetask.library

import android.util.Log
import timber.log.Timber


class CrashReportingTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {

        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return
        }

        if (t != null) {
            if (priority == Log.ERROR) {
                Timber.e(javaClass.simpleName, t.toString())
            } else if (priority == Log.WARN) {
                Timber.w(javaClass.simpleName, t.toString())
            }
        }
    }

}