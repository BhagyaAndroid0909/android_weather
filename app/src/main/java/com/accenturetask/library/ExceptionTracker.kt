package com.accenturetask.library


object ExceptionTracker {

    fun track(exception: Exception) {
        exception.printStackTrace()
    }

}