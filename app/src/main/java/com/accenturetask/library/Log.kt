package com.accenturetask.library

import timber.log.Timber

object Log {

    //debug logs
    fun d(value: String) {
        Timber.d(value)
    }

    //info logs
    fun i(value: String) {
        Timber.i(value)
    }

    //error logs
    fun e(value: String) {
        Timber.e(value)
    }

}