# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Android application to save weather conditions for your current location and search city weather
* App version: 1.0

### Project contains ###

* Language: Kotlin
* Dependencies
1) Material dependencies- To support Dark Theme
2) Timber - Debug logger
3) Retrofit - Api integration
4) Moshi - JSON type converter
5) Jetpack components - To support MVVM  pattern
6) RxAndroid and Rx java - To get Live user data
7) dagger - instance injector
8) Gms play services - To get current location
9) Multidex
* Database configuration
Room Database - To save the weather data in local storage
Version - 1

### Non- functional requirements ###

* This app compatibility with Android 4.1 and onwards
* MVVM pattern, readability
* Used Kotlin language
* Best UI practices, Material design used
* Local Storage used to store the correct location weather list, It can be accessed even app is offline
* Basic Unit test class
### Optional requirements ###
* UI layout are optimized for both Phone and Tablets screen
* Implemented More details screen, 
- When user click on any item from the list, It will redirect to detail screen
- Detail screen contains, City Name, Temperature, Min Temperature, Max Temperature, Country code, 
Humidity, Feel like in Temperature, Pressure
* Using search option , Can search any city and view the weather details in detail screen
* Weather data can refresh periodically by 10 minutes 
* CRUD operations - Read all the weather list data, Update the data and created the weather list data

### Sample screenshot and Sample video for more reference ###
![picture](WeatherList.png)
![picture](WeatherDetail.png)

